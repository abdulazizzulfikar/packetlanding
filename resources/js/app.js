// import the dependencies
require('./bootstrap');
window.Vue = require('vue');
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';

// list of components
Vue.component('landing-component', require('./components/landingComponent.vue').default);
// export the app
new Vue({
    el: '#app'
});
