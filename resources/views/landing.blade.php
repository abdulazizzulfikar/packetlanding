<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Packet - Bare Metal Server & On Premise Cloud Provider</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
    <div id="app">
        <landing-component public-path="{{ asset('/') }}"></landing-component>
    </div>
    <script src="{{asset('js/app.js')}}"></script>
</body>
</html>